//
//  ContentView.swift
//  HouseStream
//
//  Created by Rocio Barramuño on 13-05-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
